# Description List Field

The Description List Field module provides the Description list field type, as well as a default widget and formatter.
It provides a basic twig template for rendering, which can easily be overridden.

## Contributing

Please read [the full documentation](https://github.com/openeuropa/openeuropa) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the available versions, see the [tags on this repository](https://github.com/openeuropa/description_list_field/tags).
